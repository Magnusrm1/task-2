﻿using System;
using System.Net.NetworkInformation;
using System.Threading;

namespace DotNetTask2
{
    class Program
    {
        public static string GetLength()
        {
            Console.WriteLine("Write a number to choose the length of your square.");
            return Console.ReadLine();
        }
        public static string GetHeight()
        {
            Console.WriteLine("Write a number to choose the height of your square.");
            return Console.ReadLine();
        }

        public static void PrintSquare(int length, int height)
        {
            // Checking if the values entered are negative.
            if (length < 0 || height < 0)
            {
                throw new ArgumentException("Length or height cannot be negative.");
            }

            String line = "";
            for (int i = 1; i <= height; i++)
            {
                // Empty the line before writing a new one.
                line = "";

                // Write a line.
                for (int j = 1; j <= length; j++)
                {
                    if (i == 1 || j == 1)
                    {
                        line += "# ";
                    }
                    else if (i == height || j == length)
                    {
                        line += "# ";
                    }
                    else
                    {
                        line += "  ";
                    }
                }
                Console.WriteLine(line);
            }
        }

        static void Main(string[] args)
        {
            while (true)
            {
                String length = GetLength();
                String height = GetHeight();
                int lengthNum = 0;
                int heightNum = 0;

                // Attempt to convert user input to integer.
                try
                {
                    lengthNum = Convert.ToInt32(length);
                    heightNum = Convert.ToInt32(height);

                    PrintSquare(lengthNum, heightNum);

                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
